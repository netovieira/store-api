# Store API - Desafio SkyHub

### Documentação:

* [Clique para visualizar!](https://documenter.getpostman.com/view/5283754/SVfJVBcm?version=latest)

### Funcionalidades:

* Categoria - cadastro, consulta, alteração e exclusão;
* Produto - cadastro, consulta, alteração e exclusão;
* Pedido - cadastro, consulta e ateração;
* Cliente - cadastro e consulta;
* Ticket Médio - consulta;
* Estado - consulta;
* Cidade - consulta;

### Tecnologias:

* Linguagem de programação Java;
* Framewok Spring Boot; 
* Banco de dados MySQL;
* Versionamento com GIT.

### Ferramentas:

* IDE Spring Tool Suite;
* Aplicativo Postman;
* SGBD MySQL Workbench;
* Plataforma de nuvem Heroku.

##### As tecnologias e ferramentas utilizadas foram escolhidas devido ao prazo de entrega do desafio e por possuir certo conhecimento.
