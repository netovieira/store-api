package com.netovieira.storeapi.resources.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductResourceTest {

	@Autowired
	private WebApplicationContext context;

	private MockMvc mcv;
	
	@Before
	public void setup() {
		this.mcv = MockMvcBuilders.webAppContextSetup(this.context).build();
	}
	
	@Test
	public void test01SucessFindById() throws Exception {
		String uri = "/products/1";
		this.mcv.perform(get(uri))
		.andExpect(status().isOk());
	}
	
	@Test
	public void test02FailureFindByIdNotFound() throws Exception {
		String uri = "/products/100";
		this.mcv.perform(get(uri))
		.andExpect(status().isNotFound());
	}

	@Test
	public void test03SucessFindAll() throws Exception {
		String uri = "/products/";
		this.mcv.perform(get(uri))
		.andExpect(status().isOk());
	}

	@Test
	public void test04SucessDeleteById() throws Exception {
		String uri = "/products/2";
		this.mcv.perform(delete(uri))
		.andExpect(status().isNoContent());
	}

	@Test
	public void test05FailureDeleteByIdNotFound() throws Exception {
		String uri = "/products/100";
		this.mcv.perform(delete(uri))
		.andExpect(status().isNotFound());
	}
	
	@Test
	public void test06SucessInsert() throws Exception {
		String uri = "/products/";
		this.mcv.perform(post(uri)	
		.content("{\"name\":\"Mac Book Pro 15\",\"description\":\"Mac Book Pro 15\","
				+ "\"price\":11160.0,\"quantityStock\":5}")
		.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isCreated())
		.andDo(MockMvcResultHandlers.print());
	}
	
	@Test
	public void test07FailureInsertPriceEmpty() throws Exception {
		String uri = "/products/";
		this.mcv.perform(post(uri)
		.content("{\"name\":\"Iphone XS\",\"description\":\"Iphone XS\","
		+ "\"price\":5850.0,\"quantityStock\":}")
		.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isBadRequest())
		.andDo(MockMvcResultHandlers.print());
	}

	@Test
	public void test08SucessUpdate() throws Exception {
		String uri = "/products/5";
		this.mcv.perform(post(uri)
		.content("{\"name\":\"Sabonete\",\"description\":\"Sabonete Líquido\","
		+ "\"price\":15.0,\"quantityStock\":10}")
		.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isNoContent())
		.andDo(MockMvcResultHandlers.print());
	}
	
	@Test
	public void test09FailureUpdateFieldDescriptionInvalid() throws Exception {
		String uri = "/products/";
		this.mcv.perform(post(uri)
		.content("{\"name\":\"Microondas\",\"descr\":\"Microondas Electrolux\","
		+ "\"price\":445.0,\"quantityStock\":3}")
		.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isBadRequest())
		.andDo(MockMvcResultHandlers.print());
	}

}