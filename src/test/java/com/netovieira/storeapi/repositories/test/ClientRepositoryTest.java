package com.netovieira.storeapi.repositories.test;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.netovieira.storeapi.domain.ClientDomain;
import com.netovieira.storeapi.repositories.ClientRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ClientRepositoryTest {

	@Autowired
	private ClientRepository clientRepository;
	
	@Test
	public void findByEmail1() {
		ClientDomain client = clientRepository.findByEmail("maria@gmail.com");
		assertThat(client).isNotNull();
	}
	
	@Test
	public void findByEmail2() {
		ClientDomain client = clientRepository.findByEmail("teste@gmail.com");
		assertThat(client).isNull();
	}
	
	@Test
	public void findByCpfOrCnpj() {
		ClientDomain client = clientRepository.findByCpfOrCnpj("407.567.070-80");
		assertThat(client).isNotNull();
		assertThat(client.getName()).isEqualTo("José Paulo");
	}
	
	@Test
	public void findByCpfOrCnpj2() {
		ClientDomain client = clientRepository.findByCpfOrCnpj("89.843.105/0001-16");
		assertThat(client).isNull();
	}
	
}