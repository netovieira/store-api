package com.netovieira.storeapi.repositories.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.netovieira.storeapi.domain.CityDomain;
import com.netovieira.storeapi.repositories.CityRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CityRespositoryTest {

	@Autowired
	private CityRepository cityRepository;
	
	@Test
	public void findByState1() {
		List<CityDomain> cities = cityRepository.findByState(1);
		assertThat(cities.size()).isEqualTo(1);
	}
	
	@Test
	public void findByState0() {
		List<CityDomain> cities = cityRepository.findByState(4);
		assertThat(cities.size()).isEqualTo(0);
	}
	
}