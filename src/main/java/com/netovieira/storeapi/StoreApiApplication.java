package com.netovieira.storeapi;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StoreApiApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(StoreApiApplication.class, args);
	}
	 
	@Override
	public void run(String... args) throws Exception {}
}