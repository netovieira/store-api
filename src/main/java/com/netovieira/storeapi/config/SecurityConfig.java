package com.netovieira.storeapi.config;

import java.util.Arrays;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.netovieira.storeapi.services.validation.AverageTicketValidator;
import com.netovieira.storeapi.services.validation.ClientFieldsValidator;
import com.netovieira.storeapi.services.validation.OrderFieldsValidator;
import com.netovieira.storeapi.services.validation.ProductFieldsValidator;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled=true)
public class SecurityConfig 
	extends WebSecurityConfigurerAdapter {

	private static final String[] PUBLIC_MATCHERS = {
			"/h2-console/**",
			"/products/**",
			"/categories/**",
			"/states/**",
			"/orders/**",
			"/clients/**"
	};
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {		 
		http.cors();
		http.csrf().disable();
		http.headers().frameOptions().disable();
		http.authorizeRequests()
			.antMatchers(PUBLIC_MATCHERS).permitAll()
			.anyRequest().permitAll();

		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}
	
	@Bean
	public CorsConfigurationSource cosConfigurationSource() {
		CorsConfiguration configuration = new CorsConfiguration().applyPermitDefaultValues();
		configuration.setAllowedMethods(Arrays.asList("POST", "PUT", "GET", "DELETE", "OPTIONS"));
		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return source;
	}
	
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	public ClientFieldsValidator clientFieldsValidator() {
		return new ClientFieldsValidator();
	}
	
	@Bean
	public OrderFieldsValidator orderFieldsValidator() {
		return new OrderFieldsValidator();
	}
	
	@Bean
	public AverageTicketValidator averageTicketValidator() {
		return new AverageTicketValidator();
	}
	
	@Bean
	public ProductFieldsValidator productFieldsValidator() {
		return new ProductFieldsValidator();
	}

}