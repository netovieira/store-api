package com.netovieira.storeapi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netovieira.storeapi.domain.BankSlipPaymentDomain;
import com.netovieira.storeapi.domain.CardPaymentDomain;

@Configuration
public class JacksonConfig {
	@Bean
	public Jackson2ObjectMapperBuilder objectMapperBuilder() {
		Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder() {
			
			public void configure(ObjectMapper objectMapper) {
				objectMapper.registerSubtypes(CardPaymentDomain.class);
				objectMapper.registerSubtypes(BankSlipPaymentDomain.class);
				super.configure(objectMapper);
			}
		};
		
		return builder;
	}
}