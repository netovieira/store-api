package com.netovieira.storeapi.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netovieira.storeapi.domain.AddressDomain;
import com.netovieira.storeapi.repositories.AddressRepository;
import com.netovieira.storeapi.services.exception.ObjectNotFoundException;

@Service
public class AddressService {

	@Autowired
	private AddressRepository addressRepository;

	public AddressDomain findById(Integer id) {
		Optional<AddressDomain> address = addressRepository.findById(id);
		return address.orElseThrow(() -> new ObjectNotFoundException(
				"Endereço não encontrado! ID: " + id));
	}
	
}