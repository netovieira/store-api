package com.netovieira.storeapi.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.netovieira.storeapi.domain.AddressDomain;
import com.netovieira.storeapi.domain.CityDomain;
import com.netovieira.storeapi.domain.ClientDomain;
import com.netovieira.storeapi.domain.enuns.ClientTypeEnum;
import com.netovieira.storeapi.dto.ClientNewDTO;
import com.netovieira.storeapi.repositories.AddressRepository;
import com.netovieira.storeapi.repositories.ClientRepository;
import com.netovieira.storeapi.services.exception.ObjectNotFoundException;
import com.netovieira.storeapi.services.validation.ClientFieldsValidator;

@Service
public class ClientService {

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Autowired
	private ClientRepository clientRepository;
	
	@Autowired
	private AddressRepository addressRepository;
	
	@Autowired
	private ClientFieldsValidator clientFieldsValidator;

	public ClientDomain findById(Integer id) {
		Optional<ClientDomain> client = clientRepository.findById(id);
		return client.orElseThrow(() -> new ObjectNotFoundException(
				"Cliente não encontrado! Id: " + id));
	}
	
	public ClientDomain findByEmail(String email) {
		ClientDomain client = clientRepository.findByEmail(email);
		if(client==null) {
			throw new ObjectNotFoundException(
				"Cliente não encontrado! Email: " + email);
		}
		return client;
	}

	public List<ClientDomain> findAll() {
		return clientRepository.findAll();
	}
	
	@Transactional
	public ClientDomain insert(ClientDomain client) {
		client.setId(null);
		client = clientRepository.save(client);
		addressRepository.saveAll(client.getAdresses());
		return client;
	}
	
	public ClientDomain fromDTO(ClientNewDTO clientDTO) {
		clientFieldsValidator.validFields(clientDTO);
		
		ClientDomain client = new ClientDomain(null, clientDTO.getName(),
				clientDTO.getEmail(), clientDTO.getCpfOrCnpj(),
				ClientTypeEnum.toEnum(clientDTO.getType()),
				passwordEncoder.encode(clientDTO.getPassword()));
		
		CityDomain city = new CityDomain(clientDTO.getCityId(),null, null);
		
		AddressDomain address = new AddressDomain(null, clientDTO.getCep(),
				clientDTO.getStreet(), clientDTO.getNumber(), 
				clientDTO.getComplement(), clientDTO.getNeighbourhood(), city, client);
		
		client.getAdresses().add(address);
		client.getPhones().add(clientDTO.getPhone1());
		
		if(clientDTO.getPhone2() != null)
			client.getPhones().add(clientDTO.getPhone2());
			
		if(clientDTO.getPhone3() != null)
			client.getPhones().add(clientDTO.getPhone3());
		
		return client;
	}
	
}