package com.netovieira.storeapi.services.exception;

public class InvalidFieldException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public InvalidFieldException(String message) {
		super(message);
	}
	
	public InvalidFieldException(String message, Throwable cause) {
		super(message, cause);
	}

}