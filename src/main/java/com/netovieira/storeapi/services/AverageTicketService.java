package com.netovieira.storeapi.services;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netovieira.storeapi.domain.OrderDomain;
import com.netovieira.storeapi.repositories.OrderRepository;
import com.netovieira.storeapi.services.exception.ObjectNotFoundException;
import com.netovieira.storeapi.services.validation.AverageTicketValidator;

@Service
public class AverageTicketService {

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private AverageTicketValidator averageTicketValidator;
	
	public List<String> findAverageTicket(String startDate, String endDate) {
		List<Date> dates = averageTicketValidator.isValidDate(startDate, endDate);
		Date start = dates.get(0);
		Date end = dates.get(1);
		
		List<OrderDomain> orders = orderRepository.findAverageTicket(start, end);
		if(orders.size() < 1)
			throw new  ObjectNotFoundException("Nehum registro encontrado "
					+ "para o período informado!");
		
		double amount = 0.0;
		int orderQuantity = 0;
				
		for (OrderDomain order : orders) {
			amount += order.getAmount();
			orderQuantity++;
		}
		
		double averageTicket = amount / orderQuantity;
		
		String format = "R$ #,##0.00";
        DecimalFormat decimal = new DecimalFormat(format);
        String ticketFormat = decimal.format(averageTicket);
        String amountFormat = decimal.format(amount);
        
        List<String> layout = layoutTicket(ticketFormat, amountFormat,
        		orderQuantity, startDate, endDate);
        return  layout;
	}
	
	public List<String> layoutTicket(String ticketFormat, String amountFormat, 
			int orderQuantity, String startDate, String endDate) {
		
		String start = startDate.replaceAll("-", "/");
		String end = endDate.replaceAll("-", "/");
		String l1 = "*************************************";
		String l2 = "*********** TICKET MÉDIO ************";
		String period = "Período de " + start + " a " + end;
		String quantity = "Quantidade de pedidos: " + orderQuantity;
		String orderAmount = "Valor total: " + amountFormat;
		String info = "Ticket médio: " + ticketFormat; 
		
		List<String> layout = new ArrayList<String>();
		layout.addAll(Arrays.asList(l1, l2, l1, period, quantity, 
				orderAmount, info, l1));
        return layout;
	}
	
}