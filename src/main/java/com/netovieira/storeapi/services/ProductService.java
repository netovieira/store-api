package com.netovieira.storeapi.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netovieira.storeapi.domain.ProductDomain;
import com.netovieira.storeapi.repositories.ProductRepository;
import com.netovieira.storeapi.services.exception.DataIntegrityException;
import com.netovieira.storeapi.services.exception.ObjectNotFoundException;
import com.netovieira.storeapi.services.validation.ProductFieldsValidator;

@Service
public class ProductService {

	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private ProductFieldsValidator productFieldsValidator;
	
	public ProductDomain findById(Integer id) {
		Optional<ProductDomain> product = productRepository.findById(id);
		return product.orElseThrow(() -> new ObjectNotFoundException(
				"Produto não encontrado! Id: " + id));
	}
	
	public List<ProductDomain> findAll() {
		List<ProductDomain> products = productRepository.findAll();
		if(products.size() < 1) 
			throw new ObjectNotFoundException("Nenhum produto encontrado!");
		return products;
	}
	
	public ProductDomain insert(ProductDomain product) {
		productFieldsValidator.isNotNull(product);
		product.setId(null);
		return productRepository.save(product);
	}
	
	public ProductDomain update(ProductDomain product) {
		productFieldsValidator.isNotNull(product);
		ProductDomain newProduct = findById(product.getId());
		updateProduct(newProduct, product);
		return productRepository.save(newProduct);
	}
	
	public void updateProduct(ProductDomain newProduct, ProductDomain product) {
		newProduct.setName(product.getName());
		newProduct.setDescription(product.getDescription());
		newProduct.setPrice(product.getPrice());
		newProduct.setQuantityStock(product.getQuantityStock());
		newProduct.setDiscount(product.getDiscount());
	}
	
	public void delete(Integer id) {
		findById(id);
		try {
			productRepository.deleteById(id);
		} catch(DataIntegrityException e) {
			throw new DataIntegrityException("Não é possível deletar um produto "
					+ "que possui pedido associado!");
		}
	}

}