package com.netovieira.storeapi.services.validation;

import com.netovieira.storeapi.domain.ProductDomain;
import com.netovieira.storeapi.services.exception.InvalidFieldException;

public class ProductFieldsValidator {
	
	public ProductFieldsValidator() {}
	
	public void validFields(ProductDomain product) {			
		isNotNull(product);
	}
	
	public void isNotNull(ProductDomain product) {
		String info = "É necessário informar ";
		if(product.getName() == null)
			throw new InvalidFieldException(info + "o nome do produto!");
	
		if(product.getDescription() == null)
			throw new InvalidFieldException(info + "a descrição do produto!");
	
		if(product.getPrice() == null)
			throw new InvalidFieldException(info + "o preço do produto!");

		if(product.getQuantityStock() == null)
			throw new InvalidFieldException(info + "a quantidade em estoque!");
	}

}