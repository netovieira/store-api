package com.netovieira.storeapi.services.validation;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.netovieira.storeapi.domain.OrderDomain;
import com.netovieira.storeapi.domain.OrderItemDomain;
import com.netovieira.storeapi.domain.ProductDomain;
import com.netovieira.storeapi.services.ProductService;
import com.netovieira.storeapi.services.exception.InvalidFieldException;

public class OrderFieldsValidator {

	public OrderFieldsValidator() {}
	
	@Autowired
	private ProductService productService;
	
	public void validFields(OrderDomain order) {
		isNotNull(order);
		validQuantity(order);	
	}
	
	public void isNotNull(OrderDomain order) {
		String info = "É necessário informar ";	
			
		if(order.getClient() == null)
			throw new InvalidFieldException(info + "o ID do cliente! Caso não lembre do mesmo,"
					+ " realize o procedimento de consultar cliente por email.");
		
		if(order.getDeliveryAddress() == null) 
			throw new InvalidFieldException(info + "o ID do endereço de entrega! Caso não lembre"
					+ " do mesmo, realize o procedimento de consultar cliente.");
		
		Set<OrderItemDomain> itens = order.getItens();
	
		for (OrderItemDomain oi : itens) {
			if (oi.getProduct() == null)
				throw new InvalidFieldException(info + "no mínimo um produto!");
		}		
	}
	
	public void validQuantity(OrderDomain order) {
		Set<OrderItemDomain> itens = order.getItens();
		
		for (OrderItemDomain oi : itens) {
			Integer productId = oi.getProduct().getId();
			ProductDomain product = productService.findById(productId);
			Integer quantityStock = product.getQuantityStock();
			
			if (quantityStock < 1) {
				throw new InvalidFieldException("Produto de ID: " + oi.getProduct().getId()  
						+ " encontra-se indisponível para venda no momento!");
			}
			
			if (quantityStock < oi.getQuantity()) {
				throw new InvalidFieldException("A quantidade disponível em estoque para o produto de ID: " 
						+ oi.getProduct().getId() + " é de " + quantityStock + " unidades!"
						+ " Favor alterar a quantidade solicitada.");
			}
		}
	}
	
	public void validUpdate(OrderDomain order) {
		Set<OrderItemDomain> itens = order.getItens();
		
		for (OrderItemDomain oi : itens) {
			if (oi.getProduct() == null)
				throw new InvalidFieldException("É necessário informar no mínimo um produto!");
		}		
		
		validQuantity(order);
	}

}