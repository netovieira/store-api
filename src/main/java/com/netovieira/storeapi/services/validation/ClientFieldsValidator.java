package com.netovieira.storeapi.services.validation;

import org.springframework.beans.factory.annotation.Autowired;

import com.netovieira.storeapi.domain.enuns.ClientTypeEnum;
import com.netovieira.storeapi.dto.ClientNewDTO;
import com.netovieira.storeapi.repositories.ClientRepository;
import com.netovieira.storeapi.services.exception.InvalidFieldException;
import com.netovieira.storeapi.services.validation.utils.DocumentUtil;

public class ClientFieldsValidator {
	
	public ClientFieldsValidator() {}
	
	@Autowired
	private ClientRepository clientRepository;
	
	public void validFields(ClientNewDTO client) {
		Integer type = client.getType() != null ? client.getType() : 1;
		
		String document = client.getCpfOrCnpj() != null
				? String.valueOf(client.getCpfOrCnpj()) : null;
				
		String email = client.getEmail() != null 
				? String.valueOf(client.getEmail()) : null;
				
		isNotNull(client);
		isEmailRegistred(email);

		if(type.equals(ClientTypeEnum.PERSON.getId()))
			isValidCPF(document);
		
		if(type.equals(ClientTypeEnum.COMPANY.getId()))
			isValidCNPJ(document);			
	}
	
	public void isValidCPF(String cpf) {
		if (cpf != null && !DocumentUtil.isValidCPF(cpf.replaceAll("[^0-9]", "")))
			throw new InvalidFieldException("CPF " + cpf + " Inválido!");
		
		if(clientRepository.findByCpfOrCnpj(cpf) != null)
			throw new InvalidFieldException("CPF " + cpf + " já registrado!");
	}
	
	public void isValidCNPJ(String cnpj) {
		if(cnpj == null && !DocumentUtil.isValidCNPJ(cnpj))
			throw new InvalidFieldException("CNPJ " + cnpj + " Inválido!");
		
		if(clientRepository.findByCpfOrCnpj(cnpj) != null)
			throw new InvalidFieldException("CNPJ " + cnpj + " já registrado!");		
	}
			
	public void isEmailRegistred(String email) {
		if(clientRepository.findByEmail(email) != null)
			throw new InvalidFieldException("Email " + email + " já registrado!");
	}
	
	public void isNotNull(ClientNewDTO client) {
		String info = "É necessário informar ";
		if(client.getName() == null)
			throw new InvalidFieldException(info + "um nome!");
	
		if(client.getEmail() == null)
			throw new InvalidFieldException(info + "um email!");

		if(client.getCpfOrCnpj() == null)
			throw new InvalidFieldException(info + "um CPJ ou CNPJ!");

		if(client.getPassword() == null)
			throw new InvalidFieldException(info + "uma senha!");

		if(client.getPhone1() == null && client.getPhone2() == null 
				&& client.getPhone3() == null)
			throw new InvalidFieldException(info + "no mínimo um telefone!");

		if(client.getCep() == null)
			throw new InvalidFieldException(info + "o CEP!");
		
		if(client.getStreet() == null)
			throw new InvalidFieldException(info + "o endereço de entrega!");

		if(client.getNumber() == null)
			throw new InvalidFieldException(info + "o número do endereço!");

		if(client.getCityId() == null)
			throw new InvalidFieldException(info + "o Id da cidade!"
					+ " Para conseguir o Id da cidade, realize a consulta de estados através da URL: "
					+ " https://store-db-b2w.herokuapp.com/states ");
	}

}