package com.netovieira.storeapi.services.validation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.netovieira.storeapi.services.exception.InvalidFieldException;

public class AverageTicketValidator {
	
	public List<Date> isValidDate(String startDate, String endDate) {		
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		List<Date> dates = new ArrayList<Date>();
		
		try {
			Date start = format.parse(startDate);
			Date end = format.parse(endDate);
			
			if (start.after(end))
				throw new InvalidFieldException("A data inicial não pode"
						+ " ser maior que a data final!");
			
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(end);
			calendar.add(Calendar.DATE, +1);
			Date end1 = calendar.getTime();
			
			dates.addAll(Arrays.asList(start, end1));
		} catch (ParseException e) {
			throw new InvalidFieldException("Data inválida! "
					+ "Favor informar no formato DD-MM-AAAA.");
		}
		return dates; 
	}	
	
}