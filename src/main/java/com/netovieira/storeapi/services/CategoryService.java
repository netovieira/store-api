package com.netovieira.storeapi.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.netovieira.storeapi.domain.CategoryDomain;
import com.netovieira.storeapi.repositories.CategoryRepository;
import com.netovieira.storeapi.services.exception.DataIntegrityException;
import com.netovieira.storeapi.services.exception.ObjectNotFoundException;

@Service
public class CategoryService {

	@Autowired
	private CategoryRepository categoryRepository;
	
	public CategoryDomain findById(Integer id) {
		Optional<CategoryDomain> category = categoryRepository.findById(id);
		return category.orElseThrow(() -> new ObjectNotFoundException(
				"Categoria não encontrada! Id: " + id));
	}
	
	public List<CategoryDomain> findAll() {
		return categoryRepository.findAll();
	}
	
	public CategoryDomain insert(CategoryDomain category) {
		category.setId(null);
		return categoryRepository.save(category);
	}
	
	public CategoryDomain update(CategoryDomain category) {
		CategoryDomain newCategory = findById(category.getId());
		updateCategory(newCategory, category);
		return categoryRepository.save(newCategory);
	}	
	
	private void updateCategory(CategoryDomain newCategory, 
			CategoryDomain category) {
		newCategory.setName(category.getName());
	}
	
	public void delete(Integer id) {
		findById(id);
		try {
			categoryRepository.deleteById(id);
		} catch(DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possível deletar uma categoria"
					+ " que possui produto associado!");
		}
	}
	
}