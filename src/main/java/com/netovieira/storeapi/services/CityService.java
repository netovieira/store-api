package com.netovieira.storeapi.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netovieira.storeapi.domain.CityDomain;
import com.netovieira.storeapi.repositories.CityRepository;
import com.netovieira.storeapi.services.exception.ObjectNotFoundException;

@Service
public class CityService {
	
	@Autowired
	private CityRepository cityRepository;
	
	public List<CityDomain> findByState(Integer stateId) {
		List<CityDomain> cities = cityRepository.findByState(stateId); 
		if(cities.size() < 1)
			throw new ObjectNotFoundException("Nenhuma cidade encontrada!");
		return cities;
	}
	
}