package com.netovieira.storeapi.services;

import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.netovieira.storeapi.domain.BankSlipPaymentDomain;

@Service
public class BankSlipService {

	public void fillBankSlip(BankSlipPaymentDomain payment, Date orderInstant) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(orderInstant);
		calendar.add(Calendar.DAY_OF_MONTH, 7);
		payment.setDueDate(calendar.getTime());
	}
	
	public void bankSlipPaid(BankSlipPaymentDomain payment) {
		Calendar calendar = Calendar.getInstance();
		payment.setPayday(calendar.getTime());
	}
	
}