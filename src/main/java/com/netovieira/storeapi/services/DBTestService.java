package com.netovieira.storeapi.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netovieira.storeapi.domain.AddressDomain;
import com.netovieira.storeapi.domain.BankSlipPaymentDomain;
import com.netovieira.storeapi.domain.CardPaymentDomain;
import com.netovieira.storeapi.domain.CategoryDomain;
import com.netovieira.storeapi.domain.CityDomain;
import com.netovieira.storeapi.domain.ClientDomain;
import com.netovieira.storeapi.domain.OrderDomain;
import com.netovieira.storeapi.domain.OrderItemDomain;
import com.netovieira.storeapi.domain.PaymentDomain;
import com.netovieira.storeapi.domain.ProductDomain;
import com.netovieira.storeapi.domain.StateDomain;
import com.netovieira.storeapi.domain.enuns.ClientTypeEnum;
import com.netovieira.storeapi.domain.enuns.OrderStatusEnum;
import com.netovieira.storeapi.domain.enuns.StatesEnum;
import com.netovieira.storeapi.repositories.AddressRepository;
import com.netovieira.storeapi.repositories.CategoryRepository;
import com.netovieira.storeapi.repositories.CityRepository;
import com.netovieira.storeapi.repositories.ClientRepository;
import com.netovieira.storeapi.repositories.OrderItemRepository;
import com.netovieira.storeapi.repositories.OrderRepository;
import com.netovieira.storeapi.repositories.PaymentRepository;
import com.netovieira.storeapi.repositories.ProductRepository;
import com.netovieira.storeapi.repositories.StateRepository;

@Service
public class DBTestService {

	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ClientRepository clientRepository;
	
	@Autowired
	private StateRepository stateRepository;
	
	@Autowired
	private CityRepository cityRepository;
	
	@Autowired
	private AddressRepository addressRepository;
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private PaymentRepository paymentRepository;
	
	@Autowired
	private OrderItemRepository orderItemRepository;
	
	public void instantiateTestDatabase() throws ParseException {

		CategoryDomain cat1 = new CategoryDomain(null, "Informática");
		CategoryDomain cat2 = new CategoryDomain(null, "Escritório");		
		CategoryDomain cat3 = new CategoryDomain(null, "Cama mesa e banho");
		CategoryDomain cat4 = new CategoryDomain(null, "Eletrônicos");
		CategoryDomain cat5 = new CategoryDomain(null, "Jardinagem");
		CategoryDomain cat6 = new CategoryDomain(null, "Decoração");
		CategoryDomain cat7 = new CategoryDomain(null, "Perfumaria");
		
		ProductDomain pro1 = new ProductDomain(null, "Computador", "Computador DELL Intel Core I7 8ª", 3900.0, 1, 100.0);
		ProductDomain pro2 = new ProductDomain(null, "Impressora", "Impressora Multifuncional Epson", 800.0, 3, 20.0);
		ProductDomain pro3 = new ProductDomain(null, "Mouse", "Mouse Sem Fio Microsoft", 80.0, 5, 0.0);
		ProductDomain pro4 = new ProductDomain(null, "Mesa de escritório", "Mesa de escritório com gavetas", 300.0, 1, 10.0);
		ProductDomain pro5 = new ProductDomain(null, "Toalha", "Toalha de rosto", 50.0, 2, 0.0);
		ProductDomain pro6 = new ProductDomain(null, "Colcha", "Colcha cama de casal", 200.0, 4, 0.0);
		ProductDomain pro7 = new ProductDomain(null, "TV true color", "TV true color 42 LG", 1400.0, 1, 40.0);
		ProductDomain pro8 = new ProductDomain(null, "Roçadeira", "Roçadeira Stihl Gasolina", 800.0, 0, 0.0);
		ProductDomain pro9 = new ProductDomain(null, "Abajur", "Abajour Murano", 400.0, 6, 5.0);
		ProductDomain pro10 = new ProductDomain(null, "Shampoo", "Shampoo Dove", 15.00, 8, 0.9);
		
		cat1.getProducts().addAll(Arrays.asList(pro1, pro2, pro3));
		cat2.getProducts().addAll(Arrays.asList(pro2, pro4));
		cat3.getProducts().addAll(Arrays.asList(pro5, pro6));
		cat4.getProducts().addAll(Arrays.asList(pro1, pro2, pro3, pro7));
		cat5.getProducts().addAll(Arrays.asList(pro8));
		cat6.getProducts().addAll(Arrays.asList(pro9));
		cat7.getProducts().addAll(Arrays.asList(pro10));
		
		pro1.getCategories().addAll(Arrays.asList(cat1, cat4));
		pro2.getCategories().addAll(Arrays.asList(cat1, cat2, cat4));
		pro3.getCategories().addAll(Arrays.asList(cat1, cat4));
		pro4.getCategories().addAll(Arrays.asList(cat2));
		pro5.getCategories().addAll(Arrays.asList(cat3));
		pro6.getCategories().addAll(Arrays.asList(cat3));
		pro7.getCategories().addAll(Arrays.asList(cat4));
		pro8.getCategories().addAll(Arrays.asList(cat5));
		pro9.getCategories().addAll(Arrays.asList(cat6));
		pro10.getCategories().addAll(Arrays.asList(cat7));
		
		categoryRepository.saveAll(Arrays.asList(cat1, cat2, cat3, cat4, cat5, cat6, cat7));
		productRepository.saveAll(Arrays.asList(pro1, pro2, pro3, pro4, pro5, pro6, pro7, pro8, pro9, pro10));
		
		StateDomain sta1 = new StateDomain(null, StatesEnum.PERNAMBUCO.getDescription());
		StateDomain sta2 = new StateDomain(null, StatesEnum.SAO_PAULO.getDescription());
		StateDomain sta3 = new StateDomain(null, StatesEnum.RIO_DE_JANEIRO.getDescription());
		
		CityDomain cit1 = new CityDomain(null, "Recife", sta1);
		CityDomain cit2 = new CityDomain(null, "São Paulo", sta2);
		CityDomain cit3 = new CityDomain(null, "Rio de Janeiro", sta3);
		
		sta1.getCities().addAll(Arrays.asList(cit1));
		sta2.getCities().addAll(Arrays.asList(cit2));
		sta3.getCities().addAll(Arrays.asList(cit3));
	
		stateRepository.saveAll(Arrays.asList(sta1, sta2, sta3));
		cityRepository.saveAll(Arrays.asList(cit1, cit2, cit3));
		
		ClientDomain cli1 = new ClientDomain(null, "José Paulo", "jose@gmail.com", "407.567.070-80", ClientTypeEnum.PERSON, "07080");
		ClientDomain cli2 = new ClientDomain(null, "Maria da Silva", "maria@gmail.com", "65.336.799/0001-37", ClientTypeEnum.COMPANY, "00137");
		ClientDomain cli3 = new ClientDomain(null, "Pedro Costa", "pedro@gmail.com", "431.437.430-69", ClientTypeEnum.PERSON, "43069");
		
		AddressDomain add1 = new AddressDomain(null, "50012345", "Av Norte", "685", "casa", "Santo Amaro", cit1, cli1);
		AddressDomain add2 = new AddressDomain(null, "45061561", "av Paulista", "200", "Apto. 10A", "Bela Vista", cit2, cli2);
		AddressDomain add3 = new AddressDomain(null, "60655615", "Av Brasil", "155", "casa", "Bangu", cit3, cli3);
				
		cli1.getPhones().addAll(Arrays.asList("999887455", "33554422"));
		cli1.getAdresses().addAll(Arrays.asList(add1));

		cli2.getPhones().addAll(Arrays.asList("986550097"));
		cli2.getAdresses().addAll(Arrays.asList(add2));
		
		cli3.getPhones().addAll(Arrays.asList("992667102", "22506684"));
		cli2.getAdresses().addAll(Arrays.asList(add3));
		
		clientRepository.saveAll(Arrays.asList(cli1, cli2, cli3));
		addressRepository.saveAll(Arrays.asList(add1, add2, add3));
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		OrderDomain ord1 = new OrderDomain(null, sdf.parse("06/08/2019 10:32"), 20.0, OrderStatusEnum.NEW, cli1, add1);
		OrderDomain ord2 = new OrderDomain(null, sdf.parse("08/08/2019 14:54"),	30.0, OrderStatusEnum.NEW, cli2, add2);
		OrderDomain ord3 = new OrderDomain(null, sdf.parse("11/08/2019 08:46"),	40.0, OrderStatusEnum.NEW, cli3, add3);
			
		PaymentDomain pay1 = new CardPaymentDomain(null, ord1, 4);
		PaymentDomain pay2 = new BankSlipPaymentDomain(null, ord2, sdf.parse("15/08/2019 00:00"), null);
		PaymentDomain pay3 = new CardPaymentDomain(null, ord3, 6);
		
		ord1.setPayment(pay1);
		ord2.setPayment(pay2);
		ord3.setPayment(pay3);
		
		cli1.getOrders().addAll(Arrays.asList(ord1));
		cli2.getOrders().addAll(Arrays.asList(ord2));
		cli3.getOrders().addAll(Arrays.asList(ord3));
		
		orderRepository.saveAll(Arrays.asList(ord1, ord2, ord3));
		paymentRepository.saveAll(Arrays.asList(pay1, pay2, pay3));
		
		OrderItemDomain ori1 = new OrderItemDomain(ord1, pro1, 100.0, 1, 3900.0);
		OrderItemDomain ori2 = new OrderItemDomain(ord2, pro2, 20.0, 1, 800.0);
		OrderItemDomain ori3 = new OrderItemDomain(ord2, pro4, 10.0, 1, 300.0);
		OrderItemDomain ori4 = new OrderItemDomain(ord3, pro7, 40.0, 1, 1400.0);
		OrderItemDomain ori5 = new OrderItemDomain(ord3, pro5, 0.0, 3, 50.00);
		
		ord1.getItens().addAll(Arrays.asList(ori1));
		ord2.getItens().addAll(Arrays.asList(ori2, ori3));
		ord3.getItens().addAll(Arrays.asList(ori4, ori5));
		
		pro1.getItens().addAll(Arrays.asList(ori1));
		pro2.getItens().addAll(Arrays.asList(ori2));
		pro4.getItens().addAll(Arrays.asList(ori3));
		pro5.getItens().addAll(Arrays.asList(ori5));
		pro7.getItens().addAll(Arrays.asList(ori4));
		
		orderItemRepository.saveAll(Arrays.asList(ori1, ori2, ori3, ori4, ori5));
	}
	
}