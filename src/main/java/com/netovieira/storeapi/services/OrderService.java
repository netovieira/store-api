package com.netovieira.storeapi.services;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.netovieira.storeapi.domain.BankSlipPaymentDomain;
import com.netovieira.storeapi.domain.OrderDomain;
import com.netovieira.storeapi.domain.OrderItemDomain;
import com.netovieira.storeapi.domain.ProductDomain;
import com.netovieira.storeapi.domain.enuns.OrderStatusEnum;
import com.netovieira.storeapi.domain.enuns.StatesEnum;
import com.netovieira.storeapi.repositories.OrderItemRepository;
import com.netovieira.storeapi.repositories.OrderRepository;
import com.netovieira.storeapi.repositories.PaymentRepository;
import com.netovieira.storeapi.services.exception.ObjectNotFoundException;
import com.netovieira.storeapi.services.validation.OrderFieldsValidator;

@Service
public class OrderService {

	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private ClientService clientService;
	
	@Autowired
	private BankSlipService bankSlipService;
	
	@Autowired
	private PaymentRepository paymentRepository;
	
	@Autowired
	private ProductService productService;
	 
	@Autowired
	private OrderFieldsValidator orderFieldsValidator;
	
	@Autowired
	private OrderItemRepository orderItemRepository;
	
	public OrderDomain findById(Integer id) {
		Optional<OrderDomain> order = orderRepository.findById(id);
		return order.orElseThrow(() -> new ObjectNotFoundException(
				"Pedido não encontrado! Id: " + id));
	}
	
	@Transactional
	public OrderDomain insert(OrderDomain order) {
		orderFieldsValidator.validFields(order);
		order.setId(null);
		order.setInstant(new Date(System.currentTimeMillis()));
		order.setStatus(OrderStatusEnum.NEW);
		order.getPayment().setOrder(order);
				
		if(order.getPayment() instanceof BankSlipPaymentDomain) {
			BankSlipPaymentDomain payment = (BankSlipPaymentDomain) order.getPayment();
			bankSlipService.fillBankSlip(payment, order.getInstant());
		}
		
		Integer stateId = order.getDeliveryAddress().getId();
		order.setFreightCost(freightCostCalculation(stateId));
		order.setClient(clientService.findById(order.getClient().getId()));
		order = orderRepository.save(order);
		paymentRepository.save(order.getPayment());
		
		for(OrderItemDomain oi : order.getItens()) {
			ProductDomain product = productService.findById(oi.getProduct().getId());
			product.setQuantityStock(product.getQuantityStock() - oi.getQuantity());
			productService.update(product);
			oi.setPrice(product.getPrice());
			oi.setProduct(product);
			oi.setOrder(order);
		}

		orderItemRepository.saveAll(order.getItens());
		return order;	
	}
	
	public OrderDomain update(OrderDomain order) {
		orderFieldsValidator.validUpdate(order);
		OrderDomain orderUp = findById(order.getId());
		updateOrder(orderUp, order);	
		return orderRepository.save(orderUp);
	}
	
	public void updateOrder(OrderDomain orderUp, OrderDomain order) {
		for(OrderItemDomain oi : order.getItens()) {
			ProductDomain product = productService.findById(oi.getProduct().getId());		
			product.setQuantityStock(product.getQuantityStock() - oi.getQuantity());
			productService.update(product);
			
			oi.setPrice(product.getPrice());
			oi.setProduct(product);
			oi.setOrder(orderUp);
			orderItemRepository.save(oi);
		}
	}
	
	public void updateStatus(Integer status, Integer id) {
		OrderDomain order = findById(id);
		order.setStatus(OrderStatusEnum.toEnum(status));
		
		if(order.getPayment() instanceof BankSlipPaymentDomain 
				&& status == OrderStatusEnum.APPROVED.getId()) {
			BankSlipPaymentDomain payment = (BankSlipPaymentDomain) order.getPayment();
			bankSlipService.bankSlipPaid(payment);
		}
			
		orderRepository.save(order);
	}
	
	public Double freightCostCalculation(Integer stateId) {
		Double freightCost = 0.0;
		if(stateId == StatesEnum.PERNAMBUCO.getId()) freightCost = 30.0;
		if(stateId == StatesEnum.SAO_PAULO.getId()) freightCost = 40.0;
		if(stateId == StatesEnum.RIO_DE_JANEIRO.getId()) freightCost = 50.0;
		else freightCost = 35.0;
		return freightCost;
	}
	
}