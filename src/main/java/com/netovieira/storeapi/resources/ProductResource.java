package com.netovieira.storeapi.resources;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.netovieira.storeapi.domain.ProductDomain;
import com.netovieira.storeapi.services.ProductService;

@RestController
@RequestMapping(value="/products")
public class ProductResource {

	@Autowired
	private ProductService productService;

	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<ProductDomain> findById(@PathVariable Integer id) {
		ProductDomain product = productService.findById(id);
		return ResponseEntity.ok().body(product);
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<ProductDomain>> findAll() {
		List<ProductDomain> products = productService.findAll();
		return ResponseEntity.ok().body(products);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<String> insert(@Valid @RequestBody ProductDomain product) {
		productService.insert(product);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(product.getId()).toUri();
		String sucess = "Produto cadastrado com sucesso! Id: " + product.getId();
		return ResponseEntity.created(uri).body(sucess);
	}
		
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public ResponseEntity<Void> update(@Valid @RequestBody 
			ProductDomain product, @PathVariable Integer id) {
		product.setId(id);
		productService.update(product);
		return ResponseEntity.noContent().build();
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Integer id) {
		productService.delete(id);
		return ResponseEntity.noContent().build();
	}
	
}