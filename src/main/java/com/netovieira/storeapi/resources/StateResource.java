package com.netovieira.storeapi.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.netovieira.storeapi.domain.StateDomain;
import com.netovieira.storeapi.services.StateService;

@RestController
@RequestMapping(value="/states")
public class StateResource {

	@Autowired
	private StateService stateService;

	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<StateDomain>> findAllByOrderByName() {
		List<StateDomain> states = stateService.findAllByOrderByName();
		return ResponseEntity.ok().body(states);
	}
	
}