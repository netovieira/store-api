package com.netovieira.storeapi.resources;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.netovieira.storeapi.domain.OrderDomain;
import com.netovieira.storeapi.services.OrderService;

@RestController
@RequestMapping(value="/orders")
public class OrderResource {
	
	@Autowired
	private OrderService orderService;
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<OrderDomain> findById(@PathVariable Integer id) {
		OrderDomain order = orderService.findById(id);
		return ResponseEntity.ok().body(order);
	}

	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<String> insert(@Valid @RequestBody OrderDomain order) {
		orderService.insert(order);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(order.getId()).toUri();
		
		String sucess = "Pedido cadastrado com sucesso! Id:" + order.getId();
		return ResponseEntity.created(uri).body(sucess);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public ResponseEntity<Void> update(@Valid @RequestBody 
			OrderDomain order, @PathVariable Integer id) {
		order.setId(id);
		orderService.update(order);
		return ResponseEntity.noContent().build();
	}
	
	@RequestMapping(value="/{id}/status/{status}", method=RequestMethod.PUT)
	public ResponseEntity<Void> updateStatus(@PathVariable
			Integer id, @PathVariable Integer status) {
		orderService.updateStatus(id, status);
		return ResponseEntity.noContent().build();
	}

}