package com.netovieira.storeapi.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.netovieira.storeapi.domain.CityDomain;
import com.netovieira.storeapi.services.CityService;

@RestController
@RequestMapping(value="/cities")
public class CityResource {
	
	@Autowired
	private CityService cityService;

	@RequestMapping(value="/{stateId}", method=RequestMethod.GET)
	public ResponseEntity<List<CityDomain>> findByState(@PathVariable Integer stateId) {
		List<CityDomain> cities = cityService.findByState(stateId);
		return ResponseEntity.ok().body(cities);
	}
	
}