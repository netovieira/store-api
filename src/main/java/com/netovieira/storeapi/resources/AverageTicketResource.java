package com.netovieira.storeapi.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.netovieira.storeapi.services.AverageTicketService;

@RestController
@RequestMapping(value="/ticket")
public class AverageTicketResource {

	@Autowired
	private AverageTicketService averageTicketService;

	@RequestMapping(value="/{startDate}/{endDate}", 
			method=RequestMethod.GET, consumes="application/json")
	public ResponseEntity<List<String>> findAverageTicket(
			@PathVariable String startDate, @PathVariable String endDate) {

		List<String> averageTicket = averageTicketService
				.findAverageTicket(startDate, endDate);		
		return ResponseEntity.ok().body(averageTicket);
	}
}