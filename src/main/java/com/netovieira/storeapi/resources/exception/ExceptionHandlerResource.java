package com.netovieira.storeapi.resources.exception;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.netovieira.storeapi.services.exception.DataIntegrityException;
import com.netovieira.storeapi.services.exception.InvalidFieldException;
import com.netovieira.storeapi.services.exception.ObjectNotFoundException;

@ControllerAdvice
public class ExceptionHandlerResource {

	@ExceptionHandler(ObjectNotFoundException.class)
	public ResponseEntity<StandardError> objectNotFound(
			ObjectNotFoundException e, HttpServletRequest request) {
		
		Integer status = HttpStatus.NOT_FOUND.value();
		String message = e.getMessage();
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(timestamp.getTime());
		
		StandardError error = new StandardError(status, message, date);
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
	}
	
	@ExceptionHandler(InvalidFieldException.class)
	public ResponseEntity<StandardError> invalidField(
			InvalidFieldException e, HttpServletRequest request) {
		
		Integer status = HttpStatus.BAD_REQUEST.value();
		String message = e.getMessage();
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(timestamp.getTime());
		
		StandardError error = new StandardError(status, message, date);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
	}
	
	@ExceptionHandler(DataIntegrityException.class)
	public ResponseEntity<StandardError> dataIntegrity(
			DataIntegrityException e, HttpServletRequest request) {
		
		Integer status = HttpStatus.CONFLICT.value();
		String message = e.getMessage();
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(timestamp.getTime());
		
		StandardError error = new StandardError(status, message, date);
		return ResponseEntity.status(HttpStatus.CONFLICT).body(error);
	}
	
}