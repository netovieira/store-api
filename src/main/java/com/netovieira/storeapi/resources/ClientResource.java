package com.netovieira.storeapi.resources;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.netovieira.storeapi.domain.ClientDomain;
import com.netovieira.storeapi.dto.ClientNewDTO;
import com.netovieira.storeapi.services.ClientService;

@RestController
@RequestMapping(value="/clients")
public class ClientResource {

	@Autowired
	private ClientService clientService;

	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<ClientDomain> findById(@PathVariable Integer id) {
		ClientDomain client = clientService.findById(id);
		return ResponseEntity.ok().body(client);
	}

	@RequestMapping(value="/email/{email}", method=RequestMethod.GET)
	public ResponseEntity<ClientDomain> findByEmail(@PathVariable String email) {
		ClientDomain client = clientService.findByEmail(email);
		return ResponseEntity.ok().body(client);
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<ClientDomain>> findAll() {
		List<ClientDomain> clients = clientService.findAll();
		return ResponseEntity.ok().body(clients);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<String> insert(@Valid @RequestBody ClientNewDTO clientDTO) {
		ClientDomain client = clientService.fromDTO(clientDTO);
		clientService.insert(client);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(client.getId()).toUri();
		
		String sucess = "Cliente cadastrado com sucesso! Id: " + client.getId();
		return ResponseEntity.created(uri).body(sucess);
	}
	
}