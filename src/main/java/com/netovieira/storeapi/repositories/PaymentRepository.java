package com.netovieira.storeapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.netovieira.storeapi.domain.PaymentDomain;

@Repository
public interface PaymentRepository
	extends JpaRepository<PaymentDomain, Integer> {}