package com.netovieira.storeapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.netovieira.storeapi.domain.AddressDomain;

@Repository
public interface AddressRepository
	extends JpaRepository<AddressDomain, Integer> {}