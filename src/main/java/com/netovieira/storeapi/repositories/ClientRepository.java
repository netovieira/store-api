package com.netovieira.storeapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netovieira.storeapi.domain.ClientDomain;

@Repository
public interface ClientRepository
	extends JpaRepository<ClientDomain, Integer> {

	@Transactional(readOnly=true)
	ClientDomain findByEmail(String email);

	@Transactional(readOnly=true)
	@Query("SELECT doc FROM TB_CLIENT doc WHERE doc.cpfOrCnpj = :cpf_or_cnpj")
	ClientDomain findByCpfOrCnpj(@Param("cpf_or_cnpj") String document);

}