package com.netovieira.storeapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.netovieira.storeapi.domain.ProductDomain;

@Repository
public interface ProductRepository 
	extends JpaRepository<ProductDomain, Integer> {}