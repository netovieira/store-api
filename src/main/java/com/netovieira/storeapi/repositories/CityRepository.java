package com.netovieira.storeapi.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netovieira.storeapi.domain.CityDomain;

@Repository
public interface CityRepository 
	extends JpaRepository<CityDomain, Integer> {
	
	@Transactional(readOnly=true)
	@Query("SELECT cit FROM TB_CITY cit "
			+ " WHERE cit.state.id = :state_id ORDER BY cit.name")
	public List<CityDomain> findByState(@Param("state_id") Integer stateId);

}