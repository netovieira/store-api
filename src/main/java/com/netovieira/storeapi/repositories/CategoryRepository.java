package com.netovieira.storeapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.netovieira.storeapi.domain.CategoryDomain;

@Repository
public interface CategoryRepository 
	extends JpaRepository<CategoryDomain, Integer> {}