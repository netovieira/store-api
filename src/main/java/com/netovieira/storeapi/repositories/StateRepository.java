package com.netovieira.storeapi.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netovieira.storeapi.domain.StateDomain;

@Repository
public interface StateRepository 
	extends JpaRepository<StateDomain, Integer> {
	
	@Transactional(readOnly=true)
	public List<StateDomain> findAllByOrderByName();
	
}