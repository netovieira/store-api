package com.netovieira.storeapi.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netovieira.storeapi.domain.OrderDomain;

@Repository
public interface OrderRepository 
	extends JpaRepository<OrderDomain, Integer> {
	
	@Transactional(readOnly=true)
	@Query("SELECT ord FROM TB_ORDER ord WHERE ord.instant "
			+ "BETWEEN :start_date AND :end_date")
	List<OrderDomain> findAverageTicket(
			@Param("start_date") Date startDate,
			@Param("end_date") Date endDate);
	
}