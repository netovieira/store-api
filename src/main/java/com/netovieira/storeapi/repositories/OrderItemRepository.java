package com.netovieira.storeapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.netovieira.storeapi.domain.OrderItemDomain;

@Repository
public interface OrderItemRepository
	extends JpaRepository<OrderItemDomain, Integer> {}