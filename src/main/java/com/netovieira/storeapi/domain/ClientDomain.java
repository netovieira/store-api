package com.netovieira.storeapi.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.netovieira.storeapi.domain.enuns.ClientTypeEnum;
import com.netovieira.storeapi.domain.enuns.ProfileEnum;

@Entity(name="TB_CLIENT")
public class ClientDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CLI_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="CLI_NAME")
	private String name;
	
	@Column(name="CLI_EMAIL")
	private String email;
	
	@Column(name="CLI_CPFORCNPJ")
	private String cpfOrCnpj;
	
	@Column(name="CLI_TYPE")
	private Integer type;
	
	@JsonIgnore
	@Column(name="CLI_PASSWORD")
	private String password;
	
	@OneToMany(mappedBy="client", cascade=CascadeType.ALL)
	private List<AddressDomain> adresses = new ArrayList<>();
	
	@ElementCollection
	@CollectionTable(name="PHONES")
	private Set<String> phones = new HashSet<>();
	
	@ElementCollection(fetch=FetchType.EAGER)
	@CollectionTable(name="PROFILES")
	private Set<Integer> profiles = new HashSet<>();
	
	@JsonIgnore
	@OneToMany(mappedBy="client")
	private List<OrderDomain> orders = new ArrayList<>();
	
	public ClientDomain() {
		addProfile(ProfileEnum.CLIENT);
	}

	public ClientDomain(Integer id, String name, String email, 
			String cpfOrCnpj, ClientTypeEnum type, String password) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.cpfOrCnpj = cpfOrCnpj;
		this.type = type==null ? 1 : type.getId();
		this.password = password;
		addProfile(ProfileEnum.CLIENT);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCpfOrCnpj() {
		return cpfOrCnpj;
	}

	public void setCpfOrCnpj(String cpfOrCnpj) {
		this.cpfOrCnpj = cpfOrCnpj;
	}

	public ClientTypeEnum getType() {
		return ClientTypeEnum.toEnum(type);
	}

	public void setType(ClientTypeEnum type) {
		this.type = type.getId();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<AddressDomain> getAdresses() {
		return adresses;
	}

	public void setAdresses(List<AddressDomain> adresses) {
		this.adresses = adresses;
	}

	public Set<String> getPhones() {
		return phones;
	}

	public void setPhones(Set<String> phones) {
		this.phones = phones;
	}

	public Set<ProfileEnum> getProfiles() {
		return profiles.stream().map(x -> ProfileEnum.toEnum(x))
				.collect(Collectors.toSet());
	}

	public void addProfile(ProfileEnum profile) {
		profiles.add(profile.getId());
	}

	public List<OrderDomain> getOrders() {
		return orders;
	}

	public void setOrders(List<OrderDomain> orders) {
		this.orders = orders;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClientDomain other = (ClientDomain) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ClientDomain [id=" + id + ", name=" + name 
				+ ", email=" + email + ", cpfOrCnpj=" + cpfOrCnpj 
				+ ", type="	+ type + ", password=" + password 
				+ ", adresses=" + adresses + ", phones=" + phones 
				+ ", profiles="	+ profiles + ", orders=" + orders + "]";
	}
		
}