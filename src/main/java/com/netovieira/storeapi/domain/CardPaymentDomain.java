package com.netovieira.storeapi.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonTypeName;

@Entity(name="TB_CARD_PAYMENT")
@JsonTypeName("cardPayment")
public class CardPaymentDomain 
	extends PaymentDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CAP_NUMBER_INSTALLMENTS")
	private Integer numberOfInstallments;
	
	public CardPaymentDomain() {}

	public CardPaymentDomain(Integer id, OrderDomain order, Integer numberOfInstallments) {
		super(id, order);
		this.numberOfInstallments = numberOfInstallments;
	}
	
	public Integer getNumberOfInstallments() {
		return numberOfInstallments;
	}
	
	public void setNumberOfInstallments(Integer numberOfInstallments) {
		this.numberOfInstallments = numberOfInstallments;
	}
	
}