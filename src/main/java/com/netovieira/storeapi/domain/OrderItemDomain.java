package com.netovieira.storeapi.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name="TB_ORDER_ITEM")
public class OrderItemDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonIgnore
	@EmbeddedId
	@Column(name="OXI_ID")
	private OrderItemPkDomain id = new OrderItemPkDomain();
	
	@Column(name="OXI_DISCOUNT")
	private double discount;
	
	@Column(name="OXI_QUANTITY")
	private int quantity;
	
	@Column(name="OXI_PRICE")
	private double price;
	
	public OrderItemDomain() {}

	public OrderItemDomain(OrderDomain order, ProductDomain product,
			double discount, int quantity, double price) {
		super();
		id.setOrder(order);
		id.setProduct(product);
		this.discount = discount;
		this.quantity = quantity;
		this.price = price;
	}
	
	public double getSubTotal() {
		return (price - discount) * quantity;
	}
	
	@JsonIgnore
	public OrderDomain getOrder() {
		return id.getOrder();
	}
	
	public void setOrder(OrderDomain order) {
		id.setOrder(order);
	}
	
	public ProductDomain getProduct() {
		return id.getProduct();
	}
	
	public void setProduct(ProductDomain product) {
		id.setProduct(product);
	}
	
	public OrderItemPkDomain getId() {
		return id;
	}
	
	public void setId(OrderItemPkDomain id) {
		this.id = id;
	}
	
	public double getDiscount() {
		return discount;
	}
	
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderItemDomain other = (OrderItemDomain) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "OrderItemDomain [id=" + id + ", discount=" + discount 
				+ ", quantity=" + quantity + ", price=" + price	+ "]";
	}	
	
}