package com.netovieira.storeapi.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name="TB_ADDRESS")
public class AddressDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ADD_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="ADD_CEP")
	private String cep;
	
	@Column(name="ADD_STREET")
	private String street;
	
	@Column(name="ADD_NUMBER")
	private String number;
	
	@Column(name="ADD_COMPLEMENT")
	private String complement;
	
	@Column(name="ADD_NEIGHBOURHOOD")	
	private String neighbourhood;
	
	@ManyToOne
	@JoinColumn(name="CIT_ID")
	private CityDomain city;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="CLI_ID")
	private ClientDomain client;

	public AddressDomain() {}
	
	public AddressDomain(Integer id, String cep, String street, String number, 
			String complement, String neighbourhood, CityDomain city, ClientDomain client) {
		super();
		this.id = id;
		this.cep = cep;
		this.street = street;
		this.number = number;
		this.complement = complement;
		this.neighbourhood = neighbourhood;
		this.client = client;
		this.setCity(city);
	}

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getCep() {
		return cep;
	}
	
	public void setCep(String cep) {
		this.cep = cep;
	}
	
	public String getStreet() {
		return street;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	public String getNumber() {
		return number;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	
	public String getComplement() {
		return complement;
	}
	
	public void setComplement(String complement) {
		this.complement = complement;
	}
	
	public String getNeighbourhood() {
		return neighbourhood;
	}
	
	public void setNeighbourhood(String neighbourhood) {
		this.neighbourhood = neighbourhood;
	}
	
	public CityDomain getCity() {
		return city;
	}
	
	public void setCity(CityDomain city) {
		this.city = city;
	}
	
	public ClientDomain getClient() {
		return client;
	}
	
	public void setClient(ClientDomain client) {
		this.client = client;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AddressDomain other = (AddressDomain) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AddressDomain [id=" + id + ", cep=" + cep 
				+ ", street=" + street + ", number=" + number
				+ ", complement=" + complement + ", neighbourhood=" 
				+ neighbourhood + ", city=" + city + ", client=" + client + "]";
	}

}