package com.netovieira.storeapi.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonTypeName;

@Entity(name="TB_BANKSLIPPAYMENT")
@JsonTypeName("bankSlipPayment")
public class BankSlipPaymentDomain 
	extends PaymentDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonFormat(pattern = "dd/MM/yyyy")
	@Column(name="BSP_DUEDATE")
	private Date dueDate;
	
	@JsonFormat(pattern = "dd/MM/yyyy")
	@Column(name="BSP_PAYDAY")
	private Date payday;	
	
	public BankSlipPaymentDomain() {}
	
	public BankSlipPaymentDomain(Integer id, OrderDomain order, 
			Date dueDate, Date payday) {
		super(id, order);
		this.dueDate = dueDate;
		this.payday = payday;
	}

	public Date getDueDate() {
		return dueDate;
	}
	
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	
	public Date getPayday() {
		return payday;
	}
	
	public void setPayday(Date payday) {
		this.payday = payday;
	}
		
}