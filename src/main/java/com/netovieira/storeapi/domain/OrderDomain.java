package com.netovieira.storeapi.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.netovieira.storeapi.domain.enuns.OrderStatusEnum;

@Entity(name="TB_ORDER")
public class OrderDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ORD_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm",
	locale = "pt-BR", timezone = "Brazil/East")
	@Column(name="ORD_DATE")
	private Date instant;
		
	@Column(name="ORD_FREIGHTCOST")
	private Double freightCost;
	
	@Column(name="ORD_STATUS")
	private Integer status;
	
	@OneToOne(cascade=CascadeType.ALL, mappedBy="order")
	private PaymentDomain payment;
		
	@ManyToOne
	@JoinColumn(name="CLI_ID")
	private ClientDomain client;
	
	@ManyToOne
	@JoinColumn(name="ADD_ID")
	private AddressDomain deliveryAddress;
	
	@OneToMany(mappedBy="id.order")
	private Set<OrderItemDomain> itens = new HashSet<>();
	
	public OrderDomain() {}
		
	public OrderDomain(Integer id, Date instant, Double freightCost, 
			OrderStatusEnum status,	ClientDomain client, AddressDomain deliveryAddress) {
		super();
		this.id = id;
		this.instant = instant;
		this.freightCost = freightCost;
		this.status = (status==null) ? 1 : status.getId();
		this.client = client;
		this.deliveryAddress = deliveryAddress;
	}
	
	public double getAmount() {
		double sum = 0.0;
		for(OrderItemDomain item : itens) {
			sum += item.getSubTotal();
		}
		return sum;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getInstant() {
		return instant;
	}

	public void setInstant(Date instant) {
		this.instant = instant;
	}

	public PaymentDomain getPayment() {
		return payment;
	}

	public void setPayment(PaymentDomain payment) {
		this.payment = payment;
	}

	public ClientDomain getClient() {
		return client;
	}

	public void setClient(ClientDomain client) {
		this.client = client;
	}
	
	public Double getFreightCost() {
		return freightCost;
	}
	
	public void setFreightCost(Double freightCost) {
		this.freightCost = freightCost;
	}
	
	public OrderStatusEnum getStatus() {
		return OrderStatusEnum.toEnum(status);
	}
	
	public void setStatus(OrderStatusEnum status) {
		this.status = status.getId();
	}

	public AddressDomain getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(AddressDomain deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public Set<OrderItemDomain> getItens() {
		return itens;
	}

	public void setItens(Set<OrderItemDomain> itens) {
		this.itens = itens;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderDomain other = (OrderDomain) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "OrderDomain [id=" + id + ", instant=" + instant 
				+ "status=" + status + ", payment=" + payment 
				+ ", client=" + client + ", freightCost= " + freightCost 
				+ ", deliveryAddress=" + deliveryAddress + ", itens=" + itens + "]";
	}
	
}