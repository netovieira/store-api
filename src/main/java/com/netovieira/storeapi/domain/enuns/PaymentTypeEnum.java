package com.netovieira.storeapi.domain.enuns;

public enum PaymentTypeEnum {
	CARD_CREDIT (1, "CARD_CREDIT"),
	BANK_SLIP (2, "BANK_SLIP");
	
	private Integer id;
	private String description;
	
	private PaymentTypeEnum(Integer id, String description) {
		this.id = id;
		this.description = description;
	}
	
	public Integer getId() {
		return id;
	}
	
	public String getDescription() {
		return description;
	}
	
	public static PaymentTypeEnum toEnum(Integer id) {
		
		if (id == null) return null;
		
		for (PaymentTypeEnum type: PaymentTypeEnum.values()) {
			if (id.equals(type.getId())) return type;
		}
		
		throw new IllegalArgumentException("Id Inválido " + id);
	}

}