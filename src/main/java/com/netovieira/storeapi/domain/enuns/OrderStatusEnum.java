package com.netovieira.storeapi.domain.enuns;

public enum OrderStatusEnum {

	NEW (1, "New"),
	APPROVED (2, "Approved"),
	DELIVERED (3, "Delivered"),
	CANCELED (4, "Canceled");
	
	private Integer id;
	private String description;
	
	private OrderStatusEnum(Integer id, String description) {
		this.id = id;
		this.description = description;
	}

	public Integer getId() {
		return id;
	}
	
	public String getDescription() {
		return description;
	}
	
	public static OrderStatusEnum toEnum(Integer id) {	
		if (id == null) return null;
		
		for (OrderStatusEnum status: OrderStatusEnum.values()) {
			if (id.equals(status.getId())) return status;
		}
		
		throw new IllegalArgumentException("Id Inválido " + id);
	}
	
}