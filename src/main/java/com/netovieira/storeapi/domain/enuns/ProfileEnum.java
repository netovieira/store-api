package com.netovieira.storeapi.domain.enuns;

public enum ProfileEnum {

	ADMIN (1, "ROLE_ADMIN"),
	CLIENT (2, "ROLE_CLIENT");
	
	private Integer id;
	private String description;
	
	private ProfileEnum(Integer id, String description) {
		this.id = id;
		this.description = description;
	}
	
	public Integer getId() {
		return id;
	}
	
	public String getDescription() {
		return description;
	}
	
	public static ProfileEnum toEnum(Integer id) {
		
		if (id == null) return null;
		
		for (ProfileEnum profile: ProfileEnum.values()) {
			if (id.equals(profile.getId())) return profile;
		}
		
		throw new IllegalArgumentException("Id Inválido " + id);
	}
	
}