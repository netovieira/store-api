package com.netovieira.storeapi.domain.enuns;

public enum ClientTypeEnum {

	PERSON (1, "Person"),
	COMPANY (2, "Company");
	
	private Integer id;
	private String description;
	
	private ClientTypeEnum(Integer id, String description) {
		this.id = id;
		this.description = description;
	}
	
	public Integer getId() {
		return id;
	}
	
	public String getDescription() {
		return description;
	}
	
	public static ClientTypeEnum toEnum(Integer id) {	
		if (id == null) return null;
		
		for (ClientTypeEnum type: ClientTypeEnum.values()) {
			if (id.equals(type.getId())) return type;
		}
		
		throw new IllegalArgumentException("Id Inválido " + id);
	}
	
}