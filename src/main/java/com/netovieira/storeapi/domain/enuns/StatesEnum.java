package com.netovieira.storeapi.domain.enuns;

public enum StatesEnum {

	ACRE(1, "ACRE", "AC"),
	ALAGOAS(2, "ALAGOAS", "AL"),
	AMAPA(3, "AMAPA", "AP"),
	AMAZONAS(4, "AMAZONAS", "AM"),
	BAHIA(5, "BAHIA", "BA"),
	CEARA(6, "CEARA", "CE"),
	DISTRITO_FEDERAL(7, "DISTRITO_FEDERAL", "DF"),
	ESPIRITO_SANTO(8, "ESPIRITO_SANTO", "ES"),
	GOIAS(9, "GOIAS", "GO"),
	MARANHAO(10, "MARANHAO", "MA"),
	MATO_GROSSO(11, "MATO_GROSSO", "MT"),
	MATO_GROSSO_DO_SUL(12, "MATO_GROSSO_DO_SUL", "MS"),
	MINAS_GERAIS(13, "MINAS_GERAIS", "MG"),
	PARA(14, "PARA", "PA"),
	PARAIBA(15, "PARAIBA", "PB"),
	PARANA(16, "PARANA", "PR"),
	PERNAMBUCO(17, "PERNAMBUCO", "PE"),
	PIAUI(18, "PIAUI", "PI"),
	RIO_DE_JANEIRO(19, "RIO_DE_JANEIRO", "RJ"),
	RIO_GRANDE_DO_NORTE(20, "RIO_GRANDE_DO_NORTE", "RN"),
	RIO_GRANDE_DO_SUL(21, "RIO_GRANDE_DO_SUL", "RS"),
	RONDONIA(22, "RONDONIA", "RO"),
	RORAIMA(23, "RORAIMA", "RR"),
	SANTA_CATARINA(24, "SANTA_CATARINA", "SC"),
	SAO_PAULO(25, "SAO_PAULO", "SP"),
	SERGIPE(26, "SERGIPE", "SE"),
	TOCANTINS(27, "TOCANTINS", "TO");
	
	private Integer id;
	private String description;
	private String uf;
	
	private StatesEnum(Integer id, String description, String uf) {
		this.id = id;
		this.description = description;
		this.uf = uf;
	}
	
	public Integer getId() {
		return id;
	}
	
	public String getDescription() {
		return description;
	}
	
	public String getUf() {
		return uf;
	}
	
	public void setUf(String uf) {
		this.uf = uf;
	}
	
	public static StatesEnum toEnum(Integer id) {
		
		if (id == null) return null;
		
		for (StatesEnum state: StatesEnum.values()) {
			if (id.equals(state.getId())) return state;
		}
		
		throw new IllegalArgumentException("Id Inválido " + id);
	}

}