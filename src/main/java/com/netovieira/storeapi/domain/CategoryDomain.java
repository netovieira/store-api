package com.netovieira.storeapi.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity(name="TB_CATEGORY")
public class CategoryDomain implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="CAT_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="CAT_NAME")
	private String name;
	
	@ManyToMany(mappedBy="categories")
	private List<ProductDomain> products = new ArrayList<>();
	
	public CategoryDomain() {}
	
	public CategoryDomain(Integer id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public List<ProductDomain> getProducts() {
		return products;
	}
	
	public void setProducts(List<ProductDomain> products) {
		this.products = products;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		CategoryDomain other = (CategoryDomain) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CategoryDomain [id=" + id + ", name=" + name + "]";
	}
	
}