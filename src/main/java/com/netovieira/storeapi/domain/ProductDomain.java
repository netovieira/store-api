package com.netovieira.storeapi.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name="TB_PRODUCT")
public class ProductDomain implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="PRO_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="PRO_NAME")
	private String name;
	
	@Column(name="PRO_DESCRIPTION")
	private String description;
	
	@Column(name="PRO_PRICE")
	private double price;
	
	@Column(name="PRO_QUANTITY")
	private int quantityStock;
	
	@Column(name="PRO_DISCOUNT")
	private double discount;
	
	@JsonBackReference
	@ManyToMany
	@JoinTable(name="TB_PRODUCT_CATEGORY",
		joinColumns=@JoinColumn(name="PRC_PRODUCT_ID"),
		inverseJoinColumns=@JoinColumn(name="PRC_CATEGORY_ID"))
	private List<CategoryDomain> categories = new ArrayList<>();
	
	@JsonIgnore
	@OneToMany(mappedBy="id.product")
	private Set<OrderItemDomain> itens = new HashSet<>();
	
	@JsonIgnore
	public List<OrderDomain> getOrders() {
		List<OrderDomain> orders = new ArrayList<>();
		for (OrderItemDomain ori : itens) {
			orders.add(ori.getOrder());
		}
		return orders;
	}
	
	public ProductDomain() {}

	public ProductDomain(Integer id, String name, String description,
			double price, int quantityStock, double discount) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.quantityStock = quantityStock;
		this.discount = discount;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Integer getQuantityStock() {
		return quantityStock;
	}

	public void setQuantityStock(Integer quantityStock) {
		this.quantityStock = quantityStock;
	}

	public double getDiscount() {
		return discount;
	}
	
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	
	public List<CategoryDomain> getCategories() {
		return categories;
	}

	public void setCategories(List<CategoryDomain> categories) {
		this.categories = categories;
	}

	public Set<OrderItemDomain> getItens() {
		return itens;
	}
	
	public void setItens(Set<OrderItemDomain> itens) {
		this.itens = itens;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		ProductDomain other = (ProductDomain) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProductDomain [id=" + id + ", name=" + name 
				+ ", description=" + description + ", price=" + price
				+ ", quantityStock=" + quantityStock + ", categories=" + categories 
				+ ", discount=" + discount + "]";
	}
	
}