package com.netovieira.storeapi.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.netovieira.storeapi.domain.AddressDomain;
import com.netovieira.storeapi.domain.OrderItemDomain;

public class OrderDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private Date instante;
	private String status;
	private double freightCost;
	private double amount;
	private String client;
	private AddressDomain deliveryAddress;
	private String payment;
	private Set<OrderItemDomain> itens = new HashSet<>();
	
	public OrderDTO() {}

	public OrderDTO(Integer id, Date instante, String status, double freightCost, 
			double amount, String client, AddressDomain deliveryAddress, 
			String payment, Set<OrderItemDomain> itens) {
		super();
		this.id = id;
		this.instante = instante;
		this.status = status;
		this.freightCost = freightCost;
		this.client = client;
		this.deliveryAddress = deliveryAddress;
		this.payment = payment;
		this.itens = itens;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getInstante() {
		return instante;
	}

	public void setInstante(Date instante) {
		this.instante = instante;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public double getFreightCost() {
		return freightCost;
	}

	public void setFreightCost(double freightCost) {
		this.freightCost = freightCost;
	}

	public double getAmount() {
		return amount;
	}
	
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public AddressDomain getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(AddressDomain deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public Set<OrderItemDomain> getItens() {
		return itens;
	}

	public void setItens(Set<OrderItemDomain> itens) {
		this.itens = itens;
	}

	@Override
	public String toString() {
		return "OrderDTO [id=" + id + ", instante=" + instante 
				+ ", status=" + status + ", freightCost=" + freightCost
				+ ", amount= " + amount + ", client=" + client 
				+ ", deliveryAddress=" + deliveryAddress 
				+ ", payment=" + payment + ", itens=" + itens + "]";
	}

}